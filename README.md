This is a wrapper for [pdf.js](https://www.npmjs.com/package/pdfjs-dist), that make it ease to use.

You can use this with svelte or vanilla js.

## Features
 - render pdf with text
 - zoom and page controls
 - block copy, inspect, print, ...

## How to

`npm install svelte-pdf-viewer`

### Svelte
```html
<PdfViewer file={ myFileUrl } bind:infos={ pdfInfos }/>

<script>
	import PdfViewer from 'svelte-pdf-viewer'
	let pdfInfos
	const myFileUrl = ''
</script>
```

### Vanilla
```html
<div id="container">
</div>

<script>
	import PdfViewer from 'svelte-pdf-viewer'
	import 'svelte-pdf-viewer/dist/bundle.css'
    const myFileUrl = ''
    
    new PdfViewer({
        target: document.querySelector('#container'),
        props: {
            file: myFileUrl
        }
    })
</script>


```

## Props
```
file: string - file url
infos: object - read only object with pdf viewer infos, only useful with svelte. 
    infos.totalPages: number - total of pdf pages
    infos.zoom: number - current zoom percent, min: 0.4, max 1.9
    infos.currentPage: number - current page
```

## Events
No one yet