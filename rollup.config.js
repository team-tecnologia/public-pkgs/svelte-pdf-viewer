import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import pkg from './package.json';
import image from '@rollup/plugin-image';
import css from 'rollup-plugin-css-only';
import commonjs from '@rollup/plugin-commonjs';

const name = pkg.name
	.replace(/^(@\S+\/)?(svelte-)?(\S+)/, '$3')
	.replace(/^\w/, m => m.toUpperCase())
	.replace(/-\w/g, m => m[1].toUpperCase());

export default {
	input: 'src/index.js',
	output: [
		{ file: pkg.module, 'format': 'es' },
		{ file: pkg.main, 'format': 'umd', name }
	],
	plugins: [
		css({ output: 'bundle.css' }),
		image(),
		svelte(),
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
	]
};
